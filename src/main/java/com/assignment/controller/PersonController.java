package com.assignment.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.assignment.entity.Person;
import com.assignment.repo.PersonRepo;

@Controller
public class PersonController {
	
	@GetMapping("/index")
	public String GetForm(Model model)
	{
		model.addAttribute("person", new Person());
		return "index";
	}

	@PostMapping("/savePerson")
	public String postForm(@ModelAttribute Person person, BindingResult result, Model model)
	{
		try {
		
		model.addAttribute("Person",personRepo.save(person));
		} catch(Exception e) {
			
			return "index";
		}
		
		
		return "savedperson";
	}
	
	@Autowired
	private PersonRepo personRepo;

	/*
	 * @PostMapping("/saveperson") public Person savePerson(@RequestBody Person
	 * person) { return personRepo.save(person);
	 * 
	 * }
	 */

	@PostMapping(value = "/updatedetails", consumes = MediaType.APPLICATION_XML_VALUE)
	public Person updatedetails(@RequestBody Person person) {
		Person dbPerson = personRepo.findByName(person.getName());
		if (dbPerson == null) {
			dbPerson = new Person();
		}
		dbPerson.setName(person.getName());

		if (person.getPension() != null) {
			dbPerson.setPension(person.getPension());
		}
		if (person.getAddress() != null) {
			dbPerson.setAddress(person.getAddress());
		}
		if (person.getSalary() != null) {
			dbPerson.setSalary(person.getSalary());
		}
		if (person.getPhoneNumber() != null) {
			dbPerson.setPhoneNumber(person.getPhoneNumber());
		}

		return personRepo.save(dbPerson);

	}

}
