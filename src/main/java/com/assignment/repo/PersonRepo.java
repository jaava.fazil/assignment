package com.assignment.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.assignment.entity.Person;
@Repository
public interface PersonRepo extends JpaRepository<Person, Long> {

	public Person findByName(String name);
}
